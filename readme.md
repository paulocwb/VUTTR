
# Very Useful Tools to Remember.

[![codebeat badge](https://codebeat.co/badges/4c85ac42-b81d-4432-a266-c21c4372477d)](https://codebeat.co/projects/github-com-paulocwb-vuttr-main)

A simple application that lets you make a catalog of great applications you want to remember or share with others.

Done as part of the backend challenge proposed by [BossaBox](https://www.bossabox.com).

## Node version

The node version currently is 16, make sure to install the same version or change the version in package.json.

The application may not be compatible with node versions older that 14.
## Authors

- [@Paulo Estraich](https://www.github.com/paulocwb)

  
## Contact
Check my [@About.me](https://paulo.hire-me.dev)

or just send me an email paulo@hire-me.dev 
=)

## Tech Stack

**Dependencies:** Node, Express, TypeScript, Mongoose, UUID, JsonWebToken, Cors, Helmet.

**Cors/Helmet:** Were added to provide security layer to application.

**Typescript:** Developed with Typescript so we can take advantage of the lastest Ecmascript features.

**Mongoose:** Mongo has been choose by the performance and reliability, mongoose as ODM add's great manipulation tools.

**Express:** They website description talks by themself: Fast, unopinionated, minimalist.

**UUID:** Unique, fast and human-readable identifiers.
## Documentation
The complete API Documentation:

[Documentation](https://vuttr.hire-me.dev/api/docs/)

  
## Demo
You can make request to demo hosted on heroku by the **BASE URL** below:

[Heroku demo](https://vuttr.hire-me.dev/api/v1)

Don't forget to use a tool like curl or Postman/Insomnia to make requests.

#### Warning

As the purpose of this application is just to be a demonstration, all the data contained in it will be deleted everyday at 12:00 am UTC.
## Environment Variables

In order to run this project, you will need to add the following environment variables to your .env file
or setup in your host env vars at they dashboard or CLI.

`NODE_ENV` - The environment you actually running.

`PORT` - HTTP port that the server will listen and run. 3000 by default. If you are going to host your application in cloud (AWS, GCP, Heroku, etc), they will define this variable for you.

`DB_URI` - MongoDB connection string.

`SECRETJWT` - A very secure and long string, used to encrypt and decrypt JSON Web Tokens.

`DOCS_PATH`  - URI that the API documentation will be exposed.

`DEFAULT_PATH` - The Default URI that the application routes will respond. eg: supposing that your application url is: https://myapp.com
and value in this variable is setted to: **/path/to/routes/**  
so your route will response the following url: https://myapp.com/path/to/routes/myawesomeroute

  
## Installation 

Install project with yarn

```bash 
  git clone https://github.com/paulocwb/VUTTR.git
  cd project-folder
  yarn install
  yarn start
```
    
## Features

- Save Tools you must want to remember.
- List all your saved tools.
- Filter tools save by tags.
- Delete Tools that you aren't excited about anymore.


  
## API Reference

#### Get all items

```http
  GET /api/v1/tools
```

#### Get all items with tag

```http
  GET /api/v1/tools/?tag=vscode
```

#### Also is possible to search by multiple tags. The response will include any record that matchs at least one of the given tags.

```http
  GET /api/v1/tools/?tag=mytag1&tag=vscode&tag=mytag3
```
##### Response:

    HTTP/1.1 200 OK
    Date: Wed, 12 May 2021 12:36:30 GMT
    Status: 200 OK
    Connection: keep-alive
    Content-Type: application/json; charset=utf-8
    Content-Length: 179

    [
      {
        "_id": "35875952-2e72-475f-982e-156523652a53",
        "tags": [
            "VSCODE",
            "IDE",
            "PRODUCTIVITY"
        ],
        "title": "VsCode Code editing. Redefined",
        "link": "https://code.visualstudio.com/",
        "description": "The hottest IDE at moment."
      }
    ]

  If no records were found:

    HTTP/1.1 204 OK
    Date: Wed, 12 May 2021 12:36:32 GMT
    Status: 204 No Content
    Connection: keep-alive
    Content-Length: 0

#### Insert a new tool

```http
  POST /api/v1/tools
```
#### Accepts:
application/json

#### Body:
```json
{
  "title":"Trello",
  "description":"Kanban and Sprint made easy",
  "link":"https://www.trello.com/",
  "tags":[
    "Kanban",
    "Product Roadmap",
    "productivity"
  ]
}
```

#### Response:
      Response-Type: application/json
      HTTP/1.1 201 Created
      Response content
     {
        "_id": "0e2e2cd9-e31c-4cbd-a794-86b4e5b519bc",
        "title": "Trello",
        "link": "https://www.trello.com/",
        "description": "Kanban and Sprint made easy",
        "tags": [
          "KANBAN",
          "PRODUCT ROADMAP",
          "PRODUCTIVITY"
        ],
        "createdAt": "2021-05-12T16:52:36.721Z",
        "updatedAt": "2021-05-12T16:52:36.721Z"
      }


#### Delete by Tool ID

```http
  DELETE /api/v1/tools/:ID
```

##### Success response
    HTTP/1.1 204 No Content
    Date: Wed, 12 May 2021 12:37:22 GMT
    Status: 204 No Content
    Connection: keep-alive
    Content-Length: 0

##### Error response
    HTTP/1.1 404 Not Found
    Date: Wed, 12 May 2021 12:38:49 GMT
    Status: 404 No Content
    Connection: keep-alive
    Content-Type: application/json; charset=utf-8
    Content-Length: 0

##### Response Body
 ```json
        {
            "Error":"Not found a record for id: 1234"
        }
```
