import jwt from "jsonwebtoken";
import { ICreateToken } from "./IToken";


const tokenFactory =  ()=>{

    const secret = process.env.SECRETJWT;

    const ttl = 30*60 //30 minutes expiration token.
    const create = (payload:ICreateToken)=>{

        const token = jwt.sign(payload,secret,{expiresIn:ttl});
        return token;
    };
    const verify = (token:string)=>{
        const isValid = jwt.verify(token,secret);

        return isValid;
    };
    const decode = (token:string)=>{
        const decodedToken = jwt.decode(token,{json:true});
        return decodedToken;
    };
    return {
        create,
        decode,
        verify
    }
}


export {tokenFactory}