import { ITools } from "./interface/ITools";
import { v4 as uuid} from "uuid";

export class Tools implements ITools{
    _id?: ITools['_id'];
    title: string;
    link: string;
    description: string;
    tags: string[]|string;
 
    constructor(data: ITools){
        data._id ??= uuid();
        
        Object.assign(this, data);
    }
}