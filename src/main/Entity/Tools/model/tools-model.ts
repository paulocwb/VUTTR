import mongoose from "mongoose";
import { IToolsModel } from "../interface/IToolsModel";

const toolsSchema = new mongoose.Schema(
	{
		_id: { type: String },
		title: { type: String, required: true },
		link: { type: String, required: true },
		description: { type: String },
		tags:[
			{type: String,uppercase:true}
		],
	},
	{ timestamps: true }
);

const toolsModel = mongoose.model<IToolsModel>('Tool',toolsSchema);


export { toolsModel }

