import { IToolsRepository } from "../interface/IToolsRepository";
import { Response, Request} from "express";
abstract class BaseUseCase {
    public response: Response;
    public request: Request;
    
    
    constructor(protected repository:IToolsRepository){};

    async handle (request:Request, response:Response){
        this.request = request;
        this.response = response;
        this.execute();        
    }

    abstract execute():void;


    jsonOkResponse(message:any){
        return this.response.json(message);
    };

    jsonCreatedResponse(message:any){
        return this.response.status(201).json(message);
    }

    jsonNoContentResponse(){        
        return this.response.status(204).send();
    };

    jsonBadRequest(message:any){
        return this.response.status(400).json(message)
    }
    jsonNotFoundResponse(message:any){
        this.response.status(404).json(message);
    }

}

export { BaseUseCase }