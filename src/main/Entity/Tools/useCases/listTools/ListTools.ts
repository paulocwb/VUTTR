import { IToolsRepository } from "../../interface/IToolsRepository";
import { BaseUseCase } from "../BaseUseCase";

class ListTools extends BaseUseCase {
	constructor(repository: IToolsRepository) {
		super(repository);
	}

	async execute() {
		try {
            const tag:string|string[] = this.request.query.tag as any;
            const tools = await this.repository.listTools(tag);
            if (!tools?.length) return this.jsonNoContentResponse();
			return this.jsonOkResponse(tools);
		} catch (err) {
			throw new Error(err.message);
		}
	}
}

export { ListTools };
