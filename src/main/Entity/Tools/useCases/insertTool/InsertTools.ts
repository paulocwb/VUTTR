import { IToolsRepository } from "../../interface/IToolsRepository";
import { BaseUseCase } from "../BaseUseCase";

class InsertTools extends BaseUseCase {
	static REQUIRED_BODY: string[] = ["title", "link", "description",'tags'];

	constructor(repository: IToolsRepository) {
		super(repository);
	}

	async execute() {
		const { body } = this.request;

		const missingBodyFields = this.validateBody(body);
		
        if (missingBodyFields)
			return this.jsonBadRequest({
				error: `One or more fields were missing.`,
				missing: `${missingBodyFields}`,
			});
        const {title,link,description,tags,...props} = body;

		try{
			const tool = await this.repository.insertOneTool({title,link,description,tags});
			this.jsonCreatedResponse(tool);
		}catch(err){
			throw new Error(err.message)
		}
	}

	validateBody(body: { [x: string]: any; }): string {
		const missingFields = [];

		for (const field of InsertTools.REQUIRED_BODY) {
			if (!body[field]) missingFields.push(field);
		}
		return missingFields.toString();
	}
}

export { InsertTools };
