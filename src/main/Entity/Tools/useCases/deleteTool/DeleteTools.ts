import { IToolsRepository } from "../../interface/IToolsRepository";
import { BaseUseCase } from "../BaseUseCase";

class DeleteTools extends BaseUseCase {
	constructor(repository: IToolsRepository) {
		super(repository);
	}

	async execute() {
		const { id } = this.request.params;
		try {
			if (!(await this.existTool(id)))
				return this.jsonNotFoundResponse({
					error: `Not found a record for id: ${id}`,
				});

			await this.repository.deleteOneTool(id);

			return this.jsonNoContentResponse();
		} catch (err) {
			throw new Error(err.message);
		}
	}

	async existTool(id: string): Promise<boolean> {
		const tool = await this.repository.findToolById(id);
		
		return tool ? true : false;
	}
}

export { DeleteTools }