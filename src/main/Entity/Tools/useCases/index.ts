import { ToolsRepository } from "../repository/ToolsRepository";
import { DeleteTools } from "./deleteTool/DeleteTools";
import { InsertTools } from "./insertTool/InsertTools";
import { ListTools } from "./listTools/ListTools";



const toolsRepository = new ToolsRepository();

const insertTool = new InsertTools(toolsRepository);

const listTools = new ListTools(toolsRepository);

const deleteTools = new DeleteTools(toolsRepository);


export { listTools,insertTool,deleteTools }