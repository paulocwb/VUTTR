import { ITools } from "./ITools";

export interface IToolDTO {

    title: ITools['title'];
    link: ITools['link'];
    description: ITools['description'];
    tags: ITools['tags'];

}