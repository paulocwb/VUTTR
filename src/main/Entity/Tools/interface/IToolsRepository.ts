import { IToolDTO } from "./IToolDTO";
import { ITools } from "./ITools";

export interface IToolsRepository{
    
    findToolById (id:string):Promise<ITools>;
    
    listTools(query?:string|string[]):Promise<ITools[]>;

    insertOneTool(data:IToolDTO):Promise<ITools>;

    deleteOneTool (id:string):Promise<void>;
}