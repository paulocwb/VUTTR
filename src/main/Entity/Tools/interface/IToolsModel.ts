import { ITools } from "./ITools";
import { Document } from "mongoose";

export type IToolsModel = ITools & Document