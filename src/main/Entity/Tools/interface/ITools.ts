export interface ITools{
    _id?: string;
    title: string;
    link: string;
    description: string;
    tags: any;
}