import { ITools } from "../interface/ITools";
import { IToolsRepository } from "../interface/IToolsRepository";
import { toolsModel } from "../model/tools-model";
import { Tools } from "../Tools";

class ToolsRepository implements IToolsRepository {

	async findToolById (id:string):Promise<ITools>{
		return  await toolsModel.findById(id,'-__v -createdAt -updatedAt').lean(true);
	}

	async listTools(tags:string[]): Promise<ITools[]> {		

		const filter =  tags? {tags:{$in:tags.toString().toUpperCase().split(',') }}: {}; //array of tags to search.
		
		const tools = await toolsModel.find(filter,'-__v -createdAt -updatedAt').lean(true)
		
		return tools;

	}
	async insertOneTool(data: ITools): Promise<ITools> {
		const toolClass = new Tools(data);
		const tool = await toolsModel.create(toolClass);
		return tool.toJSON({versionKey:false});
	}

	async deleteOneTool (id:string):Promise<void>{

		await toolsModel.deleteOne({_id:id});
	}
}

export { ToolsRepository };
