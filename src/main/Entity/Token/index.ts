import { CreateToken } from "./useCases/CreateToken";

const createToken = new CreateToken();

export { createToken };