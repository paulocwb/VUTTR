
import { tokenFactory } from "../../../helpers/tokens/Token";
import { TokenBase } from "./TokenBase";

class CreateToken extends TokenBase{
	

	execute(){
		try {
			const sub = Date.now().toString();
			const token = tokenFactory().create({ sub });
			return this.response.status(201).json({ token });
		} catch (err) {
			return this.response.status(400).json({ error: err.message });
		}
	}
}

export { CreateToken }