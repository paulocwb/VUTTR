import { Response, Request } from "express";

abstract class TokenBase {
	public request: Request;
	public response: Response;

	handle(request: Request, response: Response) {
		this.request = request;
		this.response = response;

		this.execute();
	}

	abstract execute(): Promise<Response> | Response<any>;
}

export { TokenBase };
