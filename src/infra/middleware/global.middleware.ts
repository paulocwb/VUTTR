import express from "express";
import cors from "cors";
import helmet from "helmet";

const middleware = {

    json: express.json(),
    cors: cors(),
    helmet:helmet(),
}

export { middleware }