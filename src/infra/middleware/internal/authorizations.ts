import { NextFunction, Request, Response } from "express";
import { JsonWebTokenError } from "jsonwebtoken";
import { tokenFactory } from "../../../main/helpers/tokens/Token";

function authRequired(
	request: Request,
	response: Response,
	next: NextFunction
) {
	const bearer = request.headers.authorization?.split(" ")[1];
	if (!bearer) response.status(401).send();

	try {
		const isValid = tokenFactory().verify(bearer);

		return next();
	} catch (err) {
		if (err instanceof JsonWebTokenError)
			return response.status(401).send();

		return response.status(500).json({ error: err.message });
	}
}

export { authRequired };
