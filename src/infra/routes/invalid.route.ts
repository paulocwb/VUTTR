import { Router } from "express";

const route = Router();

route.use("*", (request, response) => {

    const doc = process.env.DOCS_PATH
	return response
		.status(404)
		.json({
			message:
				`The resource you are trying to access does not exist or you do not have sufficient privileges.`,
            documentation:` Check documentation for further information: ${doc}`
		});
});

export default route