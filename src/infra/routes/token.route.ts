import { Router } from "express";
import { createToken } from "../../main/Entity/Token";


const route = Router();


route.get("/",(request,response)=>{
    
    createToken.handle(request,response);

})

export {route as token};