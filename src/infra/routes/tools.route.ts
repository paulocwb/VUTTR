import { Router } from "express";
import * as services from "../../main/Entity/Tools/useCases";
import { authRequired } from "../middleware/internal/authorizations";


const route = Router();

route.post("/",authRequired, (request,response)=>{
    services.insertTool.handle(request,response);
});

route.get("/",authRequired,(request,response)=>{
    services.listTools.handle(request,response);
});

route.delete("/:id",authRequired,(request,response)=>{

    services.deleteTools.handle(request,response);
    
})

export {route as tools};