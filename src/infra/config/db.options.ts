export interface IMongoOptions {
	uri: string;
	options: {
		useNewUrlParser: boolean;
		useUnifiedTopology: boolean;
		autoIndex: boolean;
		autoCreate: boolean;
		useCreateIndex: boolean;
		useFindAndModify: boolean;
	};
}
export const mongoOptions: IMongoOptions = {
	uri: process.env.DB_URI,
	options: {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		autoIndex: true,
		autoCreate: true,
		useCreateIndex: true,
		useFindAndModify: false,
	},
};
