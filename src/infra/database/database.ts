import mongoose, { Connection } from "mongoose";
import { IMongoOptions } from "../config/db.options";
 

class Database {

    
    async connection({uri, options}:IMongoOptions):Promise<Connection>{
        await mongoose.connect(uri, options);
        return mongoose.connection
    }
    
}

export { Database }