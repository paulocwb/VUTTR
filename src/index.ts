import { App } from "./application/app";
import express from "express";
import { Database } from "./infra/database/database";
import { mongoOptions } from "./infra/config/db.options"; 
import * as routes from "./infra/routes"
import errorRoute from  "./infra/routes/invalid.route";
import { middleware } from "./infra/middleware/global.middleware";
const app = new App(express());
const db = new Database();

db.connection(mongoOptions).then(()=>{
    
    
    Object.entries(middleware).forEach(mid =>{
        app.middleware(mid[1]);
    });
    
    for (const route of Object.entries(routes)){
        
        app.routes(`${process.env.DEFAULT_PATH}${route[0]}`, route[1]);
    }

    app.routes("*",errorRoute);
    
    
    app.startServer(Number(process.env.PORT)||3000);
});