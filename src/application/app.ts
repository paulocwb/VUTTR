import { RequestHandler, Router,Application } from "express";

import { swaggerUI,swaggerSetup } from "./swagger/swagger-config";
class App {
    
    public app:Application;

    constructor(app: Application){
        this.app = app;
        this.app.use(process.env.DOCS_PATH,swaggerUI.serve, swaggerUI.setup(swaggerSetup))
    }

    middleware(middleware:RequestHandler):void{
        this.app.use(middleware);
    }

    routes(path: string,route:Router):void{
        this.app.use(path,route);
    }

    startServer(port:number):void{
        this.app.listen(port, ()=>{
            console.log(`Application server running on port ${port}`);
        })
    }
}


export { App }